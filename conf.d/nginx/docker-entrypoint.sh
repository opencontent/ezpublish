#!/bin/bash
[[ -n $DEBUG ]] && set -ex

if [[ -n $EZ_INSTANCE ]]; then
  echo "==> Changing ez instance variable in ${EZ_INSTANCE} ..."
  sed -i "s/prototipo/${EZ_INSTANCE}/" /etc/nginx/conf.d/default.conf 
fi

backend_host=${NGINX_BACKEND_HOST:-php}
if [[ -n $NGINX_BACKEND_HOST ]]; then
  backend_port=${NGINX_BACKEND_PORT:-9000}
  echo "==> Changing php backend ${NGINX_BACKEND_HOST}:${backend_port} ..."
  sed -i "s/ php:9000;/ ${NGINX_BACKEND_HOST}:${backend_port};/" /etc/nginx/conf.d/default.conf 
fi

[[ $WAIT_FOR_SERVICES == 'true' ]] && wait-for-it $backend_host:$backend_port --timeout=0 --strict -- echo "PHP server @ $backend_host:$backend_port is UP"

echo "==> Starting supervisor... good luck!"
#exec /usr/local/bin/supervisord
exec $@
